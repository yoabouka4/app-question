from . import db


class Users(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    expert = db.Column(db.Integer, nullable=False)
    admin = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<User %r' % self.name


class Questions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question_text = db.Column(db.String(150), nullable=False)
    answer_text = db.Column(db.String(150))
    expert_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    asked_by_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    asker = db.relationship('Users', foreign_keys=[asked_by_id], lazy=False)
    expert = db.relationship('Users', foreign_keys=[expert_id], lazy=False)

    def __repr__(self):
        return '<Question %r' % self.id


def init_db():
    db.create_all()
