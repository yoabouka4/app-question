from flask import current_app as app
from flask import render_template, url_for, session, redirect
from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms import StringField, TextAreaField, PasswordField, SelectField, ValidationError
from wtforms.validators import InputRequired, Email

from .models import Users, Questions, db


class RegisterForm(FlaskForm):
    name = StringField('Name', validators=[InputRequired('this field is required')])
    email = StringField('Email', validators=[InputRequired('this field is required'),
                                             Email('your email is not correct , please write a correct email address')])
    password = PasswordField('Password', validators=[InputRequired('this field is required')])

    def validate_name(self, field):
        users = Users.query.order_by(Users.name).all()

        list_users = []
        for user in users:
            list_users.append(user.name)
        else:
            if list_users.__contains__(field.data):
                raise ValidationError('this username already exists')

    def validate_email(self, field):
        users = Users.query.order_by(Users.name).all()

        list_users = []
        for user in users:
            list_users.append(user.email)
        else:
            if list_users.__contains__(field.data):
                raise ValidationError('this email already exists')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired('this field is required'),
                                             Email('your email is not correct, please! write a correct email adress')])
    password = PasswordField('Password', validators=[InputRequired('this field is required')])

    def validate_email(self, field):
        users = Users.query.order_by(Users.name).all()

        list_users = []
        for user in users:
            list_users.append(user.email)
        else:
            if not list_users.__contains__(field.data):
                raise ValidationError('your email does\'t exist')


class Ask(FlaskForm):
    expert_list = []
    experts = Users.query.filter(Users.expert != 0).all()
    for expert in experts:
        expert_tuple = (int(expert.id), expert.name)
        expert_list.append(expert_tuple)

    question_text = TextAreaField('Question', validators=[InputRequired('this field is required')])
    expert_id = SelectField('Expert', coerce=int, choices=expert_list,
                            validators=[InputRequired('this field is required')])


def get_current_user():
    user_object = {}

    if 'user' in session:
        user = session['user']
        user_result = Users.query.filter(Users.name == user).first()
        user_object['id'] = user_result.id
        user_object['name'] = user_result.name
        user_object['email'] = user_result.email
        user_object['password'] = user_result.password
        user_object['expert'] = user_result.expert
        user_object['admin'] = user_result.admin

    return user_object


class Answer(FlaskForm):
    answer_text = TextAreaField('Answer', validators=[InputRequired('this field is required')])


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


@app.route('/')
@app.route('/home')
def index():
    user = get_current_user()

    q = Questions.query.filter(Questions.answer_text.isnot(None))

    questions_tab = []

    for question in q:
        questions_list = {'id': question.id, 'question_text': question.question_text,
                          'answer_text': question.answer_text, 'asker': question.asker.name,
                          'expert': question.expert.name}

        questions_tab.append(questions_list)

    return render_template('home.html', user=user, questions=questions_tab)


@app.route('/login', methods=['POST', 'GET'])
def login():
    current_user = get_current_user()

    form = LoginForm()

    errors = {'error_password': 'None', 'error_email': 'None'}

    if form.validate_on_submit():

        email = form.email.data
        password = form.password.data

        user_result = Users.query.filter(Users.email == email).first()

        if user_result:
            if check_password_hash(user_result.password, password):
                session['user'] = user_result.name
                return redirect(url_for('index'))
            else:
                errors['error_password'] = 'password do not match'

    return render_template('login.html', errors=errors, user=current_user, form=form)


@app.route('/register', methods=['POST', 'GET'])
def register():
    current_user = get_current_user()

    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        created_user = Users(password=hashed_password, expert=0, admin=0)
        form.populate_obj(created_user)
        created_user.password = hashed_password
        db.session.add(created_user)
        db.session.commit()
        session['user'] = form.name.data
        return redirect(url_for('index'))

    return render_template('register.html', user=current_user, form=form)


@app.route('/askquestion', methods=['POST', 'GET'])
def ask():
    current_user = get_current_user()

    form = Ask()

    if not current_user:
        return redirect(url_for('login'))
    if current_user['expert'] or current_user['admin']:
        return redirect(url_for('index'))

    if form.validate_on_submit():
        new_question = Questions(asked_by_id=current_user['id'])

        form.populate_obj(new_question)
        db.session.add(new_question)
        db.session.commit()

        return redirect(url_for('index'))

    experts_list = Users.query.order_by(Users.name).filter(Users.expert == 1)
    experts = []

    for expert in experts_list:
        experts_dict = {'id': expert.id, 'name': expert.name}

        experts.append(experts_dict)

    return render_template('ask.html', user=current_user, experts=experts, form=form)


@app.route('/answers/<question_id>', methods=['POST', 'GET'])
def answers(question_id):
    current_user = get_current_user()

    form = Answer()

    if not current_user:
        return redirect(url_for('login'))

    if not current_user['expert']:
        return redirect(url_for('index'))

    question_result = Questions.query.filter(Questions.id == question_id).first()

    if form.validate_on_submit():
        question_result.answer_text = form.answer_text.data
        db.session.commit()

        return redirect(url_for('unanswered'))

    return render_template('answer.html', user=current_user, question=question_result, form=form)


@app.route('/unanswered')
def unanswered():
    current_user = get_current_user()

    if not current_user:
        return redirect(url_for('login'))

    if not current_user['expert']:
        return redirect(url_for('index'))

    unanswered_questions = Questions.query.filter(db.and_(Questions.answer_text is None,
                                                          Questions.expert_id == current_user['id'])).all()

    unanswered_questions_list = []
    for question in unanswered_questions:
        unanswered_questions_dict = {'id': question.id, 'question_text': question.question_text,
                                     'asker': question.asker.name}

        unanswered_questions_list.append(unanswered_questions_dict)

    return render_template('unanswered.html', questions=unanswered_questions_list, user=current_user)


@app.route('/questions/<question_id>')
def questions(question_id):
    current_user = get_current_user()

    if not current_user:
        return redirect(url_for('login'))

    question_infos = Questions.query.filter(Questions.id == question_id).first()
    question = {}
    if question_infos:
        question = {'id': question_infos.id, 'question_text': question_infos.question_text,
                    'answer_text': question_infos.answer_text, 'asker': question_infos.asker.name,
                    'expert': question_infos.expert.name}

    return render_template('question.html', user=current_user, question=question)


@app.route('/users')
def users_control():
    current_user = get_current_user()

    if not current_user:
        return redirect(url_for('login'))

    if not current_user['admin']:
        return redirect(url_for('index'))

    users_object = Users.query.order_by(Users.id).all()

    users = []
    for user in users_object:
        user_dict = {'id': user.id, 'name': user.name, 'admin': user.admin, 'expert': user.expert}

        users.append(user_dict)

    return render_template('users.html', user=current_user, users=users)


@app.route('/promote/<user_id>')
def promote(user_id):
    current_user = get_current_user()

    if not current_user:
        return redirect(url_for('login'))
    if not current_user['admin']:
        redirect(url_for('index'))

    user_expert_to_promote = Users.query.filter(Users.id == user_id).first()

    user_expert_to_promote.expert = 1
    db.session.commit()

    return redirect(url_for('users_control'))


@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect(url_for('index'))
