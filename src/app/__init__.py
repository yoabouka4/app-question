from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import os

db = SQLAlchemy()
# db = SQLAlchemy(app)


def create_app():
    application = Flask(__name__)
    application.config['SECRET_KEY'] = os.urandom(24)
    application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://christ:u08NCWmOuWdiD93b@localhost:3306/questionapp'
    application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    application.config['WTF_CSRF_ENABLED'] = True
    application.config['WTF_CSRF_SECRET_KEY'] = os.urandom(25)
    application.config['WTF_CSRF_TIME_LIMIT'] = 3600

    db.init_app(application)

    with application.app_context():
        from . import app
        db.create_all()
    return application
